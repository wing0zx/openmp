program main
    implicit none
    integer i, j
    integer, parameter :: N = 10000
    integer, allocatable :: a(:), b(:)
    allocate(a(N), b(N))
    a = (/(i, i = 1, N)/)
    b = 0
    !$omp parallel private(j)
    !$omp do
    do i = 1, N
        j = N - i + 1
        b(j) = a(i)
    enddo
    !$omp enddo
    !$omp endparallel
    write(*,'(i0, a, i0)') sum(a), " = ", sum(b)
end program main