program main
    implicit none
    integer, parameter :: N = 10000
    real(8), allocatable :: a(:), b(:)
    allocate (a(N), b(N))
    call random_number(a)
    call random_number(b)
    !$omp parallel
    !$omp sections
    !$omp section
    call sort(a)
    !$omp section
    call sort(b)
    !$omp endsections
    !$omp endparallel
    write(*,'("first 4 numbers in a : ", 4f10.7)') a(1:4)
    write(*,'("first 4 numbers in b : ", 4f10.7)') b(1:4)

    contains
    subroutine sort(x)
        real(8) x(:), tmp
        integer i, j
        do i = 1, N - 1
            do j = i + 1, N
                if (x(i) > x(j)) then
                    tmp = x(i)
                    x(i) = x(j)
                    x(j) = tmp
                endif
            enddo
        enddo
    end subroutine sort
end program main