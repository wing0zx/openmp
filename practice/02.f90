program main
    !$ use omp_lib
    implicit none
    write(*,*) "Start here!"
    !$omp parallel
    write(*,*) "Hello!"
    !$omp end parallel
    write(*,*) "End here!"
end program main