program main
    !$ use omp_lib
    implicit none
    write(*,*) "START"
!$omp parallel
    write(*,'(a, i0, a, i0)') "Hello! N = ", omp_get_num_threads(), ", and I am ", omp_get_thread_num()
!$omp end parallel
    write(*,*) "END"
end program main