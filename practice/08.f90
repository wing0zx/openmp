program kadaiSingle
    implicit none
    integer,parameter :: N = 1000*1000
    integer i
    double precision total
    double precision,allocatable :: a(:)
    allocate( a(N) )
    print *, "Starting..."
    !$omp parallel
    !$omp do
    do i=1, N
        a(i) = i
    end do
    !$omp end do
    total = sum(a)
    !$omp single
    print *, "1st phase done..."
    !$omp endsingle
    !$omp do
    do i=1,N
        a(i) = a(i)/total
    end do
    !$omp end do
    !$omp end parallel
    print *, "All done...  Check value =", sum(a)
end program