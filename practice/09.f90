program kadaiNowait
    use omp_lib
    implicit none
    integer i, a(10), b(20)
    print *, "Before Parallel Region"
    !$omp parallel
    print *, "Before 1st Loop :", omp_get_thread_num()
    !$omp do
    do i=1,10
        a(i) = i
    end do
    !$omp end do nowait
    print *, "Between 1st and 2nd Loops :", omp_get_thread_num()
    !$omp do
    do i=1,20
        b(i) = 21-i
    end do
    !$omp end do nowait
    print *, "After 2nd Loop :", omp_get_thread_num()
    !$omp end parallel
    print *, "After Parallel Region"
    print *, "Sums are :", sum(a), sum(b)
end program