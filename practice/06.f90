program main
    !$ use omp_lib
    implicit none
    integer, parameter :: N = 1000 * 1000, M = 100
    integer i, j
    real(8), allocatable :: a(:)
    real(8) x
    !$ real(8) st, en
    allocate(a(N))

    !$ st = omp_get_wtime()
    !$omp parallel private(x)
    !$omp do
    do i = 1, N
        x = 0.0d0
        do j = 1, M
            x = x + log(dble(i + j))
        enddo
        a(i) = x / M
    enddo
    !$omp enddo
    !$omp endparallel
    !$ en = omp_get_wtime()
    write(*,*) nint(sum(a))
    !$ write(*,*) "Elapsed time : ", en - st
end program main