program main
    implicit none
    integer i, prod
    prod = 1
    !$omp parallel
    !$omp do reduction(*:prod)
    do i = 1, 10
        prod = prod * i
    enddo
    !$omp enddo
    !$omp end parallel
    write(*,*) "product = ", prod
end program main