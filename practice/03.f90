program main
    !$ use omp_lib
    implicit none
    integer, parameter :: N = 100
    integer i
    real(8) x(N)
    !$omp parallel
    !$omp do
    do i = 1, N
        x(i) = dble(i)/N
    enddo
    !$omp enddo
    !$omp end parallel
    write(*,*) sum(x)
end program main