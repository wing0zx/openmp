program main
  !$ use omp_lib
  implicit none
  real(8) x, y, pi, pi0
  integer(8) n, i, im
  integer threads
  !$ real(8) st, en
  write(*,*) "スレッド数を入力（0~8）"
  read(*,*) threads
  write(*,*) "円周率を推定する乱数の生成数（試行回数）"
  read(*,*) im
  pi0 = 2.0d0 * acos(0.0d0)
  n = 0

  !$ st = omp_get_wtime()
  !$omp parallel num_threads(threads), private(x, y), reduction(+:n)
  !$omp do
  do i = 1, im
     call random_number(x)
     call random_number(y)
     if (x ** 2 + y ** 2 <= 1.0d0) n = n + 1
  enddo
  !$omp enddo
  !$omp endparallel
  !$ en = omp_get_wtime()

  pi = 4.0d0 * dble(n) / dble(im)
  write(*,*)"円周率 ", pi0
  write(*,*)"推定円周率 ", pi
  !$ write(*,*) "Elapsed time : ", en - st
end program main